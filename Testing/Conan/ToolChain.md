# Frontend
* TypeScript
* React

# Backend
* Spring Boot
* Spring MVC
* RESTful API & Swagger

# CI + Content Collaboration
* JIRA & Confluence
* 


# IDE & Tools
* IntelliJ
* SourceTree