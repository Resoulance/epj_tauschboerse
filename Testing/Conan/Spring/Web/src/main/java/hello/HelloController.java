package hello;

import org.springframework.web.bind.annotation.*;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Greetings from Resoulance Web API";
    }


    @RequestMapping("/math/")
    public String math() {
        int n = 5;
        int product = n * n;
        return String.valueOf(product);
    }


    @RequestMapping(value = "/pow/{number}", method = RequestMethod.GET)
    @ResponseBody
    public String pow(@PathVariable final String number) {
        double tmp = Double.parseDouble(number);
        tmp = Math.pow(2.0,tmp);
        return String.valueOf(tmp);
    }


}